<?php

/**
* Plugin de puente al módulo Block
*/

/**
* @file
* Contains \Drupal\pruebas\Plugin\Block\PruebasBloque
*/

// Declarar namespace
namespace Drupal\pruebas\Plugin\Block;

// Importar clases
use Drupal\Core\Block\BlockBase; // Clase base para bloques
use Drupal\Core\Form\FormStateInterface; // Interfaz para el estado actual de un formulario
// use Drupal\Core\Database\Database; // Para uso de la base de datos


/**
* Crea un bloque
*
* @Block(
* 	id = "pruebas_bloque",
* 	admin_label = @Translation( "Pruebas: Bloque de prueba" ),
* )
*
*/
class PruebasBloque extends BlockBase
{
	/**
	* Función que genera la configuración de la clase
	*
	* {@inheritdoc}
	*
	*/
	public function defaultConfiguration()
	{
		// Retornar array con propiedades que serán accesibles para el resto de funciones
		return array(
			'pruebas_bloque_cadena' => $this -> t( 'Valor por defecto. Este bloque se creó a las %time', array( '%time' => date( 'c' ) ) ),
		);
	}
	
	
	/**
	* Función pública para construir el bloque
	*
	* {@inheritdoc}
	*
	*/
	public function build()
	{
		// Inicializar variable del contenido
		$contenido = '';
		
		// Cargar propiedad de la configuración
		$contenido .= '<p>' . $this -> configuration[ 'pruebas_bloque_cadena' ] . '</p>';
		
		// Cargar un nodo
		$nodo = entity_load( 'node', 2 );
		$contenido .= '<p><strong>' . $nodo -> title[ 0 ] -> value . '</strong></p>';
		
		// Hacer consulta de entidades
		$query = \Drupal::entityQuery( 'node' )
			-> condition( 'status', 1 );
		
		// Ejecutar consulta
		$nids = $query -> execute();
		
		// Carga múltiple de entidades
		$nodos = entity_load_multiple( 'node', $nids );
		
		// Inicializar listado
		$contenido .= '<ul>';
		
		// Hacer bucle
		foreach( $nodos as $nodo_individual ) {
			$contenido .= '<li>' . $nodo_individual -> title[ 0 ] -> value . '</li>';
		}
		
		// Finalizar listado
		$contenido .= '</ul>';
		
		
		
		// Hacer ejemplo de SELECT
		$contenido .= '<br><p><strong>Ejemplo de SELECT</strong></p>';
		
		// Hacer select
		$query = \Drupal::database() -> select( 'node', 'n' )
			-> fields( 'n', array( 'nid', 'type' ) )
			-> condition( 'nid', 0, '>' )
			-> execute();
		
		// Habilitar el conteo de filas de resultados
		$query -> allowRowCount = TRUE;
		
		// Imprimir total de resultados
		$contenido .= '<p>Total de resultados: ' . $query -> rowCount() . '</p>';
		
		// Obtener todos los resultados
		$results = $query -> fetchAll();
		
		// Inicializar listado
		$contenido .= '<ul>';
		
		// Recorrer resultados
		foreach( $results as $record ) {
			$contenido .= '<li>' . $record -> nid . ' / ' . $record -> type . '</li>';
		}
		
		// Finalizar listado
		$contenido .= '</ul>';
		
		
		
		// Hacer ejemplo de SELECT con JOIN
		$contenido .= '<br><p><strong>Ejemplo de SELECT con JOIN</strong></p>';
		
		// Hacer select con joins
		$query2 = \Drupal::database() -> select( 'node', 'n' );
		$query2 -> join( 'node_field_data', 'nfd', 'nfd.nid = n.nid' );
		$query2 -> fields( 'n', array( 'nid', 'type' ) )
			-> fields( 'nfd', array( 'title', 'uid' ) )
			-> condition( 'n.nid', 0, '>' );
		
		// Ejecutar consulta
		$data = $query2 -> execute();
		
		// Habilitar el conteo de filas de resultados
		$data -> allowRowCount = TRUE;
		
		// Imprimir total de resultados
		$contenido .= '<p>Total de resultados de SELECT con JOIN: ' . $data -> rowCount() . '</p>';
		
		// Obtener todos los resultados
		$results2 = $data -> fetchAll();
		
		// Inicializar listado
		$contenido .= '<ul>';
		
		// Recorrer resultados
		foreach( $results2 as $record2 ) {
			$contenido .= '<li>' . $record2 -> nid . ' / ' . $record2 -> type . ' / ' .
							$record2 -> title . ' / ' . $record2 -> uid . '</li>';
		}
		
		// Finalizar listado
		$contenido .= '</ul>';
		
		/*
		
		// SELECT WITH JOINS
		$query = \Drupal::database() -> select( 'node', 'n' )
			-> join( 'users_field_data', 'ufd', 'ufd.uid = nfd.uid' )
			-> fields( 'n', array( 'nid', 'type' ) )
			-> condition( 'nid', 0, '>' )
			-> execute();
		
		// INSERT
		$query = \Drupal::database() -> insert( 'tabla' )
			-> fields( array(
				'campo1' => 'valor1',
				'campo2' => 'valor2',
				'campo3' => 'valor3',
			) )
			-> execute();
		
		// UPDATE
		$query = \Drupal::database() -> update( 'tabla' )
			-> fields( array(
				'campo1' => 'valor1',
				'campo2' => 'valor2',
				'campo3' => 'valor3',
			) )
			-> condition( 'id', '1', '=' )
			-> execute();
		
		// DELETE
		$query = \Drupal::database() -> delete( 'tabla' )
			-> condition( 'id', '1', '=' )
			-> execute();
		
		*/
		
		
		
		// Devuelve arreglo de contenido
		return array(
			'#type' => 'markup',
			'#markup' => $contenido,
		);
	}
	
	
	/**
	*
	* Función que crea un formulario para el bloque
	*
	* {@inheritdoc}
	*
	*/
	public function blockForm( $form, FormStateInterface $form_state )
	{
		// Llamar a la función del padre
		$form = parent::blockForm( $form, $form_state );
		
		// Crear campo de texto
		$form[ 'nombre_campo' ] = array(
			'#type' => 'textarea',
			'#title' => $this -> t( 'Título del campo' ),
			'#description' => $this -> t( 'Descripción del campo' ),
			'#default_value' => $this -> configuration[ 'pruebas_bloque_cadena' ],
		);
		
		// Retornar formulario
		return $form;
	}
	
	
	/**
	*
	* Función para enviar el formulario
	*
	* {@inheritdoc}
	*
	*/
	public function blockSubmit( $form, FormStateInterface $form_state )
	{
		// Llamar a la función del padre
		parent::blockSubmit( $form, $form_state );
		
		// Sustituir el valor de la configuración por el enviado en el formulario
		$this -> configuration[ 'pruebas_bloque_cadena' ] = $form_state -> getValue( 'nombre_campo' );
	}
}

?>